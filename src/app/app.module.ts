// Importamos la clase 'NgModule' desde la librería '@angular/core' para definir un módulo de Angular
import { NgModule } from '@angular/core';

// Importamos el módulo 'BrowserModule' desde la librería '@angular/platform-browser'
// BrowserModule es necesario para que nuestra aplicación se ejecute en un navegador web
import { BrowserModule } from '@angular/platform-browser';

// Importamos el módulo 'HttpClientModule' desde la librería '@angular/common/http'
// HttpClientModule proporciona funcionalidades para realizar peticiones HTTP en nuestra aplicación
import { HttpClientModule } from '@angular/common/http';

// Importamos el módulo de rutas 'AppRoutingModule' que definimos previamente
// Este módulo contiene la configuración de las rutas de nuestra aplicación
import { AppRoutingModule } from './app-routing.module';

// Importamos los componentes que se utilizarán en la aplicación
import { AppComponent } from './app.component';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { NosotrosComponent } from './paginas/nosotros/nosotros.component';
import { ContactenosComponent } from './paginas/contactenos/contactenos.component';
import { PublicacionesComponent } from './paginas/publicaciones/publicaciones.component';

// Importamos el servicio 'PublicacionesService' que definimos previamente
// Este servicio nos permite obtener datos de las publicaciones desde una API
import { PublicacionesService } from './servicios/publicaciones.service';

// Definimos el módulo principal de la aplicación llamado 'AppModule'
@NgModule({
  // En el bloque 'declarations', enumeramos todos los componentes que pertenecen a este módulo
  // Estos componentes serán utilizados en las plantillas HTML de nuestra aplicación
  declarations: [
    AppComponent,
    InicioComponent,
    NosotrosComponent,
    ContactenosComponent,
    PublicacionesComponent
  ],

  // En el bloque 'imports', importamos otros módulos que necesitamos en este módulo
  // BrowserModule nos permite ejecutar la aplicación en el navegador
  // AppRoutingModule contiene la configuración de las rutas de nuestra aplicación
  // HttpClientModule nos proporciona funcionalidades para realizar peticiones HTTP
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],

  // En el bloque 'providers', registramos los servicios que vamos a inyectar en nuestra aplicación
  // PublicacionesService es un servicio que nos permite obtener datos de las publicaciones desde una API
  providers: [PublicacionesService],

  // En el bloque 'bootstrap', especificamos el componente raíz de la aplicación
  // AppComponent es el componente principal que se carga cuando se inicia la aplicación
  bootstrap: [AppComponent]
})

// Exportamos el módulo 'AppModule'
export class AppModule { }
