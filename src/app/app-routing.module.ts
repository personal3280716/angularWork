// Importamos la clase 'NgModule' desde la librería '@angular/core' para definir un módulo de Angular
import { NgModule } from '@angular/core';

// Importamos la clase 'RouterModule' y 'Routes' desde la librería '@angular/router' para manejar las rutas de la aplicación
import { RouterModule, Routes } from '@angular/router';

// Importamos los componentes que se utilizarán en las rutas
import { InicioComponent } from './paginas/inicio/inicio.component';
import { NosotrosComponent } from './paginas/nosotros/nosotros.component';
import { ContactenosComponent } from './paginas/contactenos/contactenos.component';
import { PublicacionesComponent } from './paginas/publicaciones/publicaciones.component';

// Definimos las rutas de la aplicación
const routes: Routes = [
  { path: 'inicio', component: InicioComponent },  // Ruta para el componente InicioComponent
  { path: 'nosotros', component: NosotrosComponent },  // Ruta para el componente NosotrosComponent
  { path: 'contactenos', component: ContactenosComponent },  // Ruta para el componente ContactenosComponent
  { path: 'publicaciones', component: PublicacionesComponent },  // Ruta para el componente PublicacionesComponent
  { path: '', redirectTo: '/inicio', pathMatch: 'full' },  // Redirecciona a la ruta '/inicio' por defecto.
];

// Definimos un módulo de rutas llamado 'AppRoutingModule'
@NgModule({
  imports: [RouterModule.forRoot(routes)],  // Importamos el módulo de rutas y configuramos las rutas definidas
  exports: [RouterModule]  // Exportamos el módulo de rutas para que pueda ser utilizado en otros lugares de la aplicación
})
// Exportamos el módulo 'AppRoutingModule'
export class AppRoutingModule { }
