// Importamos la clase 'Injectable' desde la librería '@angular/core'
// Esto nos permite inyectar dependencias en nuestro servicio
import { Injectable } from '@angular/core';

// Importamos la clase 'HttpClient' desde la librería '@angular/common/http'
// Esta clase nos permite realizar peticiones HTTP.
import { HttpClient } from '@angular/common/http';

// Importamos la clase 'Observable' desde la librería 'rxjs'
// Usamos Observable para manejar flujos asincrónicos de datos
import { Observable } from 'rxjs';

// Definimos un nuevo servicio llamado 'PublicacionesService'
@Injectable({
  providedIn: 'root'  // Indicamos que este servicio estará disponible en toda la aplicación
})
export class PublicacionesService {
  private apiUrl = 'https://jsonplaceholder.typicode.com/posts';  // URL de la API de publicaciones

  // Constructor del servicio, inyectamos el servicio HttpClient para realizar peticiones HTTP
  constructor(private http: HttpClient) { }

  // Método para obtener las publicaciones
  // Devuelve un Observable que emite un arreglo de objetos con ciertas propiedades
  getPublicaciones(): Observable<{ userId: number, id: number, title: string, body: string }[]> {
    // Realizamos una solicitud GET a la API y esperamos un arreglo con las propiedades especificadas
    return this.http.get<{ userId: number, id: number, title: string, body: string }[]>(this.apiUrl);
  }
}
