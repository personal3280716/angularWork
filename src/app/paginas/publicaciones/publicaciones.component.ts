// Importamos la clase 'Component' desde la libreria '@angular/core'
import { Component } from '@angular/core'
// Importamos el servicio 'PublicacionesService' desde la ruta '../../servicios/publicaciones.service'
import { PublicacionesService } from '../../servicios/publicaciones.service'

// Definimos un nuevo componente llamado 'app-publicaciones'
@Component({
  // El selector define como usaremos este componente en el HTML, en este caso, lo llamaremos con <app-publicaciones>
  selector: 'app-publicaciones',
  // Especificamos la plantilla HTML que se utilizara para este componente
  templateUrl: './publicaciones.component.html',
  // Especificamos la hoja de estilos CSS que se aplicara a este componente
  styleUrl: './publicaciones.component.css'
})

// Esta es la clase del componente 'PublicacionesComponent'
export class PublicacionesComponent {
  // Creamos un arreglo vacio para almacenar las publicaciones
  publicaciones: any[] = []

  // Constructor del componente, inyectamos el servicio 'PublicacionesService'
  constructor(private publicacionesService: PublicacionesService) {
    // Llamamos al método 'getPublicaciones' del servicio y suscribimos para obtener los datos
    this.publicacionesService.getPublicaciones().subscribe(data => {
      // Asignamos los datos obtenidos al arreglo de publicaciones
      this.publicaciones = data
    })
  }
}
