// Importamos la clase 'NgModule' desde la libreria '@angular/core' para poder definir un modulo de Angular
import { NgModule } from '@angular/core'
// Importamos el modulo 'CommonModule' desde la libreria '@angular/common' para poder utilizar directivas como *ngFor, *ngIf, etc
import { CommonModule } from '@angular/common'

// Definimos un nuevo modulo llamado 'PaginasModule' para agrupar componentes, directivas y pipes relacionados
@NgModule({
  // Aqui se declaran los componentes, directivas y pipes que pertenecen a este modulo, en este caso no hay ninguno
  declarations: [],
  // Importamos otros modulos que necesitamos en este modulo, en este caso importamos CommonModule para su uso en las plantillas HTML
  imports: [
    CommonModule
  ]
})
// Exportamos el modulo 'PaginasModule' para que pueda ser utilizado en otros lugares de la aplicacion
export class PaginasModule { }
