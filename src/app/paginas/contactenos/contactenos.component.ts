// Importamos la clase 'Component' desde la librería '@angular/core'
// Esta clase es esencial para definir componentes en Angular
import { Component } from '@angular/core';

// Definimos un nuevo componente llamado 'app-contactenos'
// Un componente en Angular es una pieza reutilizable de la interfaz de usuario que agrupa la funcionalidad, la estructura y los estilos relacionados
@Component({
  // El selector define cómo usaremos este componente en el HTML
  // Al establecerlo como 'app-contactenos', podremos insertar este componente en cualquier parte de nuestro HTML usando <app-contactenos>
  selector: 'app-contactenos',
  
  // Especificamos la plantilla HTML que se utilizará para este componente
  // La plantilla HTML define la estructura y el contenido visual del componente
  templateUrl: './contactenos.component.html',
  
  // Especificamos la hoja de estilos CSS que se aplicará a este componente
  // Esta hoja de estilos define la apariencia visual y los estilos de nuestro componente
  styleUrl: './contactenos.component.css'
})
// Esta es la clase del componente 'ContactenosComponent'
// Aquí definimos la lógica y el comportamiento del componente
export class ContactenosComponent {
  // En esta sección, podemos agregar lógica adicional para nuestro componente si es necesario
  // Por ejemplo, podríamos definir propiedades, métodos o eventos específicos del componente aquí
}
